Public network functions in ioquake3 1.36 revision 1582
*******************************************************

These functions are declared in 'qcommon/qcommon.h'.

net_ip.c
********

NET_AdrToString
---------------
client/cl_curl.c
client/cl_main.c
client/cl_ui.c
qcommon/net_chan.c
server/sv_ccmds.c
server/sv_client.c
server/sv_main.c

NET_AdrToStringwPort
--------------------
client/cl_main.c
client/cl_ui.c
server/sv_main.c

NET_CompareAdr
--------------
client/cl_main.c
client/cl_ui.c
server/sv_client.c

NET_CompareBaseAdrMask
----------------------
server/sv_ccmds.c
server/sv_client.c

NET_CompareBaseAdr
------------------
server/sv_client.c
server/sv_main.c

NET_IsLocalAddress
------------------
client/cl_main.c
server/sv_client.c

NET_JoinMulticast6
------------------
server/sv_init.c

NET_LeaveMulticast6
-------------------
server/sv_init.c

NET_Sleep
---------
server/sv_main.c

Sys_GetPacket
-------------
null/null_net.c
null/mac_net.c
qcommon/common.c

Sys_IsLANAddress
----------------
client/cl_input.c
client/cl_main.c
server/sv_client.c
server/sv_snapshot.c

Sys_SendPacket
--------------
null/null_net.c
null/mac_net.c
qcommon/net_chan.c

Sys_ShowIP
----------
client/cl_main.c

Sys_StringToAdr
---------------
qcommon/net_chan.c

net_chan.c
**********

NET_FlushPacketQueue
--------------------
qcommon/common.c

NET_GetLoopPacket
-----------------
qcommon/common.c

NET_OutOfBandData
-----------------
client/cl_main.c

NET_OutOfBandPrint
------------------
client/cl_main.c
server/sv_ccmds.c
server/sv_client.c
server/sv_main.c

NET_SendPacket
--------------
client/cl_main.c
qcommon/net_ip.c

NET_StringToAdr
---------------
client/cl_main.c
client/cl_ui.c
null/null_net.c
null/mac_net.c
server/sv_ccmds.c
server/sv_client.c
server/sv_main.c

Netchan_Init
------------
qcommon/common.c

Netchan_Process
---------------
client/cl_net_chan.c
server/sv_net_chan.c

Netchan_Setup
-------------
client/cl_main.c
server/sv_client.c

Netchan_TransmitNextFragment
----------------------------
client/cl_net_chan.c
qcommon/qcommon.h:
server/sv_net_chan.c

Netchan_Transmit
----------------
client/cl_net_chan.c
qcommon/qcommon.h:
server/sv_net_chan.c:
