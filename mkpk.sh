#!/bin/sh
# Creates a PAK file 'pak1.pk3' containing the VM modules of the game
# Argument: build directory path (relative to current)
# Note: This is only for use as a command inside the Makefile.

CWD=`pwd`
BD=$CWD/$1
ZIP=$CWD/misc/zip.exe
DATE=`date`
PAKID=`./misc/mkrand.exe 8`

echo "Creating 'pak1.pk3'... in" $1

echo "Platform: Win32/MinGW" > $BD/baseqm/vm/pakid
echo "Build date:" $DATE >> $BD/baseqm/vm/pakid
echo "ID:" $PAKID >> $BD/baseqm/vm/pakid

cd $BD/baseqm
$ZIP pak1.zip ./vm/cgame.qvm ./vm/qagame.qvm ./vm/ui.qvm ./vm/pakid
mv pak1.zip $BD/pak1.pk3
cd $CWD
