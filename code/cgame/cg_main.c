/*
===========================================================================
Copyright (C) 1999-2005 Id Software, Inc.

This file is part of Quake III Arena source code.

Quake III Arena source code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Quake III Arena source code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quake III Arena source code; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
===========================================================================
*/

// _ML_unlagged_done

//
// cg_main.c -- initialization and primary entry point for cgame
#include "cg_local.h"

// _ML_ added - exe_checking
#include "../../version.h"
// _ML_ added end

// _ML_ added - crosshair_fix
static char *crosshair_num[NUM_CROSSHAIRS] = {
  "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
};

// _ML_ added end

// _ML_ added - crosshair_color
int colored_crosshair = 0;
vec4_t cross_color = { 1.0, 1.0, 1.0, 1.0 };

// _ML_ added end

// _ML_ added - pm_skins
int pm_skin_colors[PM_COLOR_NUM][3] = {
  // skin (for heads)
  {210, 180, 150},

  // red
  {100, 0, 0},
  {120, 0, 0},
  {140, 0, 0},
  {160, 0, 0},
  {180, 0, 0},
  {100, 0, 0},
  {220, 0, 0},
  {240, 0, 0},

  // green
  {0, 100, 0},
  {0, 120, 0},
  {0, 140, 0},
  {0, 160, 0},
  {0, 180, 0},
  {0, 200, 0},
  {0, 220, 0},
  {0, 240, 0},

  // blue
  {0, 0, 100},
  {0, 0, 120},
  {0, 0, 140},
  {0, 0, 160},
  {0, 0, 180},
  {0, 0, 200},
  {0, 0, 220},
  {0, 0, 240},

  // yellow
  {100, 80, 0},
  {120, 100, 0},
  {140, 120, 0},
  {160, 140, 0},
  {180, 160, 0},
  {200, 180, 0},
  {220, 200, 0},
  {240, 220, 0},

  // CPMA style pure colors
  {0, 0, 0},
  {255, 0, 0},
  {0, 255, 0},
  {255, 255, 0},
  {0, 0, 255},
  {0, 255, 255},
  {255, 0, 255},
  {255, 255, 255},
};

int pm_enemy_color = DEFAULT_ENEMY_COLOR;
int pm_team_color = DEFAULT_TEAM_COLOR;
// _ML_ added end

int forceModelModificationCount = -1;

void CG_Init(int serverMessageNum, int serverCommandSequence, int clientNum);
void CG_Shutdown(void);

/*
================
vmMain

This is the only way control passes into the module.
This must be the very first function compiled into the .q3vm file
================
*/
intptr_t
vmMain(int command, int arg0, int arg1, int arg2, int arg3, int arg4,
       int arg5, int arg6, int arg7, int arg8, int arg9, int arg10, int arg11)
{

  switch (command)
  {
  case CG_INIT:
    CG_Init(arg0, arg1, arg2);
    return 0;
  case CG_SHUTDOWN:
    CG_Shutdown();
    return 0;
  case CG_CONSOLE_COMMAND:
    return CG_ConsoleCommand();
  case CG_DRAW_ACTIVE_FRAME:
    CG_DrawActiveFrame(arg0, arg1, arg2);
    return 0;
  case CG_CROSSHAIR_PLAYER:
    return CG_CrosshairPlayer();
  case CG_LAST_ATTACKER:
    return CG_LastAttacker();
  case CG_KEY_EVENT:
    CG_KeyEvent(arg0, arg1);
    return 0;
  case CG_MOUSE_EVENT:
    CG_MouseEvent(arg0, arg1);
    return 0;
  case CG_EVENT_HANDLING:
    CG_EventHandling(arg0);
    return 0;
  default:
    CG_Error("vmMain: unknown command %i", command);
    break;
  }
  return -1;
}

//==============================================================================

// _ML_ added - fem_announcer
vmCvar_t cg_femaleAnnouncer;

enum
{
  VOC_TEAM_ATTACK,
  VOC_TEAM_BLU_RET,
  VOC_TEAM_BLU_SCORE,
  VOC_TEAM_ENEMY_1FLAG,
  VOC_TEAM_ENEMY_FLAG,
  VOC_TEAM_RED_RET,
  VOC_TEAM_RED_SCORE,
  VOC_TEAM_TEAM_FLAG,
  VOC_TEAM_TEAM_1FLAG,
  VOC_TEAM_YOU_FLAG,
  VOC_TEAM_NUM
};

static char *voc_team_dir = "/sound/teamplay/";

static char *voc_teamplay[VOC_TEAM_NUM] = {
  "voc_base_attack.wav",
  "voc_blue_returned.wav",
  "voc_blue_scores.wav",
  "voc_enemy_1flag.wav",
  "voc_enemy_flag.wav",
  "voc_red_returned.wav",
  "voc_red_scores.wav",
  "voc_team_1flag.wav",
  "voc_team_flag.wav",
  "voc_you_flag.wav"
};


enum
{
  VOC_FEED_1FRAG,
  VOC_FEED_1MIN,
  VOC_FEED_2FRAG,
  VOC_FEED_3FRAG,
  VOC_FEED_5MIN,
  VOC_FEED_ASSIST,
  VOC_FEED_BLU_LEAD,
  VOC_FEED_DEFENSE,
  VOC_FEED_DENIED,
  VOC_FEED_EXCELLENT,
  VOC_FEED_FIGHT,
  VOC_FEED_FRAGS,
  VOC_FEED_HUMILIATION,
  VOC_FEED_IMPRESSIVE,
  VOC_FEED_LOST_LEAD,
  VOC_FEED_ONE,
  VOC_FEED_PERFECT,
  VOC_FEED_PREPARE,
  VOC_FEED_RED_LEAD,
  VOC_FEED_SUDDEN,
  VOC_FEED_TAKE_LEAD,
  VOC_FEED_TEAMS_TIED,
  VOC_FEED_THREE,
  VOC_FEED_TIED,
  VOC_FEED_TWO,
  VOC_FEED_HOLYSHIT,
  VOC_FEED_NUM
};

static char *voc_feed_dir = "/sound/feedback/";

static char *voc_feedback[] = {
  "1_frag.wav",
  "1_minute.wav",
  "2_frags.wav",
  "3_frags.wav",
  "5_minute.wav",
  "assist.wav",
  "blueleads.wav",
  "defense.wav",
  "denied.wav",
  "excellent.wav",
  "fight.wav",
  "frags.wav",
  "humiliation.wav",
  "impressive.wav",
  "lostlead.wav",
  "one.wav",
  "perfect.wav",
  "prepare.wav",
  "redleads.wav",
  "sudden_death.wav",
  "takenlead.wav",
  "teamstied.wav",
  "three.wav",
  "tiedlead.wav",
  "two.wav",
  "voc_holyshit.wav"
};

// _ML_ added end

cg_t cg;
cgs_t cgs;
centity_t cg_entities[MAX_GENTITIES];
weaponInfo_t cg_weapons[MAX_WEAPONS];
itemInfo_t cg_items[MAX_ITEMS];

vmCvar_t cg_railTrailTime;
vmCvar_t cg_centertime;
vmCvar_t cg_runpitch;
vmCvar_t cg_runroll;
vmCvar_t cg_bobup;
vmCvar_t cg_bobpitch;
vmCvar_t cg_bobroll;
vmCvar_t cg_swingSpeed;
vmCvar_t cg_shadows;
vmCvar_t cg_gibs;
vmCvar_t cg_drawTimer;
vmCvar_t cg_drawFPS;
vmCvar_t cg_drawSnapshot;
vmCvar_t cg_draw3dIcons;
vmCvar_t cg_drawIcons;
vmCvar_t cg_drawAmmoWarning;
vmCvar_t cg_drawCrosshair;
vmCvar_t cg_drawCrosshairNames;
vmCvar_t cg_drawRewards;
vmCvar_t cg_crosshairSize;
vmCvar_t cg_crosshairX;
vmCvar_t cg_crosshairY;
vmCvar_t cg_crosshairHealth;
vmCvar_t cg_draw2D;
vmCvar_t cg_drawStatus;
vmCvar_t cg_animSpeed;
vmCvar_t cg_debugAnim;
vmCvar_t cg_debugPosition;
vmCvar_t cg_debugEvents;
vmCvar_t cg_errorDecay;
vmCvar_t cg_nopredict;
vmCvar_t cg_noPlayerAnims;
vmCvar_t cg_showmiss;
vmCvar_t cg_footsteps;
vmCvar_t cg_addMarks;
vmCvar_t cg_brassTime;
vmCvar_t cg_viewsize;
vmCvar_t cg_drawGun;
vmCvar_t cg_gun_frame;
vmCvar_t cg_gun_x;
vmCvar_t cg_gun_y;
vmCvar_t cg_gun_z;
vmCvar_t cg_tracerChance;
vmCvar_t cg_tracerWidth;
vmCvar_t cg_tracerLength;
vmCvar_t cg_autoswitch;
vmCvar_t cg_ignore;
vmCvar_t cg_simpleItems;
vmCvar_t cg_fov;
vmCvar_t cg_zoomFov;
vmCvar_t cg_thirdPerson;
vmCvar_t cg_thirdPersonRange;
vmCvar_t cg_thirdPersonAngle;
vmCvar_t cg_lagometer;
vmCvar_t cg_drawAttacker;
vmCvar_t cg_synchronousClients;
vmCvar_t cg_teamChatTime;
vmCvar_t cg_teamChatHeight;
vmCvar_t cg_stats;
vmCvar_t cg_buildScript;
vmCvar_t cg_forceModel;
vmCvar_t cg_paused;
vmCvar_t cg_blood;
vmCvar_t cg_predictItems;
vmCvar_t cg_deferPlayers;
vmCvar_t cg_drawTeamOverlay;
vmCvar_t cg_teamOverlayUserinfo;
vmCvar_t cg_drawFriend;
vmCvar_t cg_teamChatsOnly;
vmCvar_t cg_noVoiceChats;
vmCvar_t cg_noVoiceText;
vmCvar_t cg_hudFiles;
vmCvar_t cg_scorePlum;

//unlagged- smooth clients #2
//unlagged this is done server-side now
// vmCvar_t cg_smoothClients;
//unlagged- smooth clients #2

vmCvar_t pmove_fixed;
vmCvar_t pmove_msec;
vmCvar_t cg_pmove_msec;
vmCvar_t cg_cameraMode;
vmCvar_t cg_cameraOrbit;
vmCvar_t cg_cameraOrbitDelay;
vmCvar_t cg_timescaleFadeEnd;
vmCvar_t cg_timescaleFadeSpeed;
vmCvar_t cg_timescale;
vmCvar_t cg_smallFont;
vmCvar_t cg_bigFont;
vmCvar_t cg_noTaunt;
vmCvar_t cg_noProjectileTrail;
vmCvar_t cg_oldRail;
vmCvar_t cg_oldRocket;
vmCvar_t cg_oldPlasma;
vmCvar_t cg_trueLightning;

//unlagged - client options
vmCvar_t cg_delag;
vmCvar_t cg_cmdTimeNudge;
vmCvar_t sv_fps;
vmCvar_t cg_projectileNudge;
vmCvar_t cg_optimizePrediction;
vmCvar_t cl_timeNudge;
//unlagged - client options

// _ML_ added - new_zoom
vmCvar_t cg_newZoom;
// _ML_ added end

// _ML_ added - weapon_select
vmCvar_t cg_newWeaponSelect;
vmCvar_t cg_weaponRank;
vmCvar_t cg_weaponGroup1;
vmCvar_t cg_weaponGroup2;
vmCvar_t cg_weaponGroup3;
vmCvar_t cg_weaponGroup4;
// _ML_ added end

// _ML_ added - no_itembob
vmCvar_t cg_noBob;
// _ML_ added end

// _ML_ added - no_medals
vmCvar_t cg_noMedals;
// _ML_ added end

// _ML_ added - crosshair_color
vmCvar_t cg_crosshairColor;
// _ML_ added end

//mathieu - show_date_time
vmCvar_t cg_drawDateTime;
//mathieu

//uZu - frag_message
vmCvar_t cg_fragString;
//uZu

//a_clown - speed_meter
vmCvar_t cg_drawSpeed;
//a_clown

//hit_sound
vmCvar_t cg_hitsound;
//hit_sound

// _ML_ added - pm_skins
vmCvar_t cg_enemyColors;
vmCvar_t cg_teamColors;
vmCvar_t cg_enemyModel;
vmCvar_t cg_teamModel;
// _ML_ added end

typedef struct
{
  vmCvar_t *vmCvar;
  char *cvarName;
  char *defaultString;
  int cvarFlags;
} cvarTable_t;

static cvarTable_t cvarTable[] = {
  {&cg_ignore, "cg_ignore", "0", 0},	// used for debugging
  {&cg_autoswitch, "cg_autoswitch", "1", CVAR_ARCHIVE},
  {&cg_drawGun, "cg_drawGun", "1", CVAR_ARCHIVE},
  {&cg_zoomFov, "cg_zoomfov", "22.5", CVAR_ARCHIVE},
  {&cg_fov, "cg_fov", "90", CVAR_ARCHIVE},
  {&cg_viewsize, "cg_viewsize", "100", CVAR_ARCHIVE},
  {&cg_shadows, "cg_shadows", "1", CVAR_ARCHIVE},
  {&cg_gibs, "cg_gibs", "1", CVAR_ARCHIVE},
  {&cg_draw2D, "cg_draw2D", "1", CVAR_ARCHIVE},
  {&cg_drawStatus, "cg_drawStatus", "1", CVAR_ARCHIVE},
  {&cg_drawTimer, "cg_drawTimer", "0", CVAR_ARCHIVE},
  {&cg_drawFPS, "cg_drawFPS", "0", CVAR_ARCHIVE},
  {&cg_drawSnapshot, "cg_drawSnapshot", "0", CVAR_ARCHIVE},
  {&cg_draw3dIcons, "cg_draw3dIcons", "1", CVAR_ARCHIVE},
  {&cg_drawIcons, "cg_drawIcons", "1", CVAR_ARCHIVE},
  {&cg_drawAmmoWarning, "cg_drawAmmoWarning", "1", CVAR_ARCHIVE},
  {&cg_drawAttacker, "cg_drawAttacker", "1", CVAR_ARCHIVE},
  {&cg_drawCrosshair, "cg_drawCrosshair", "4", CVAR_ARCHIVE},
  {&cg_drawCrosshairNames, "cg_drawCrosshairNames", "1", CVAR_ARCHIVE},
  {&cg_drawRewards, "cg_drawRewards", "1", CVAR_ARCHIVE},
  {&cg_crosshairSize, "cg_crosshairSize", "24", CVAR_ARCHIVE},
  {&cg_crosshairHealth, "cg_crosshairHealth", "1", CVAR_ARCHIVE},
  {&cg_crosshairX, "cg_crosshairX", "0", CVAR_ARCHIVE},
  {&cg_crosshairY, "cg_crosshairY", "0", CVAR_ARCHIVE},
  {&cg_brassTime, "cg_brassTime", "2500", CVAR_ARCHIVE},
  {&cg_simpleItems, "cg_simpleItems", "0", CVAR_ARCHIVE},
  {&cg_addMarks, "cg_marks", "1", CVAR_ARCHIVE},
  {&cg_lagometer, "cg_lagometer", "1", CVAR_ARCHIVE},
  {&cg_railTrailTime, "cg_railTrailTime", "400", CVAR_ARCHIVE},
  {&cg_gun_x, "cg_gunX", "0", CVAR_CHEAT},
  {&cg_gun_y, "cg_gunY", "0", CVAR_CHEAT},
  {&cg_gun_z, "cg_gunZ", "0", CVAR_CHEAT},
  {&cg_centertime, "cg_centertime", "3", CVAR_CHEAT},
  {&cg_runpitch, "cg_runpitch", "0.002", CVAR_ARCHIVE},
  {&cg_runroll, "cg_runroll", "0.005", CVAR_ARCHIVE},
  {&cg_bobup, "cg_bobup", "0.005", CVAR_CHEAT},
  {&cg_bobpitch, "cg_bobpitch", "0.002", CVAR_ARCHIVE},
  {&cg_bobroll, "cg_bobroll", "0.002", CVAR_ARCHIVE},
  {&cg_swingSpeed, "cg_swingSpeed", "0.3", CVAR_CHEAT},
  {&cg_animSpeed, "cg_animspeed", "1", CVAR_CHEAT},
  {&cg_debugAnim, "cg_debuganim", "0", CVAR_CHEAT},
  {&cg_debugPosition, "cg_debugposition", "0", CVAR_CHEAT},
  {&cg_debugEvents, "cg_debugevents", "0", CVAR_CHEAT},
  {&cg_errorDecay, "cg_errordecay", "100", 0},
  {&cg_nopredict, "cg_nopredict", "0", 0},
  {&cg_noPlayerAnims, "cg_noplayeranims", "0", CVAR_CHEAT},
  {&cg_showmiss, "cg_showmiss", "0", 0},
  {&cg_footsteps, "cg_footsteps", "1", CVAR_CHEAT},
  {&cg_tracerChance, "cg_tracerchance", "0.4", CVAR_CHEAT},
  {&cg_tracerWidth, "cg_tracerwidth", "1", CVAR_CHEAT},
  {&cg_tracerLength, "cg_tracerlength", "100", CVAR_CHEAT},
  {&cg_thirdPersonRange, "cg_thirdPersonRange", "40", CVAR_CHEAT},
  {&cg_thirdPersonAngle, "cg_thirdPersonAngle", "0", CVAR_CHEAT},
  {&cg_thirdPerson, "cg_thirdPerson", "0", 0},
  {&cg_teamChatTime, "cg_teamChatTime", "3000", CVAR_ARCHIVE},
  {&cg_teamChatHeight, "cg_teamChatHeight", "0", CVAR_ARCHIVE},
  {&cg_forceModel, "cg_forceModel", "0", CVAR_ARCHIVE},
  {&cg_predictItems, "cg_predictItems", "1", CVAR_ARCHIVE},
  {&cg_deferPlayers, "cg_deferPlayers", "1", CVAR_ARCHIVE},
  {&cg_drawTeamOverlay, "cg_drawTeamOverlay", "0", CVAR_ARCHIVE},
  {&cg_teamOverlayUserinfo, "teamoverlay", "0", CVAR_ROM | CVAR_USERINFO},
  {&cg_stats, "cg_stats", "0", 0},
  {&cg_drawFriend, "cg_drawFriend", "1", CVAR_ARCHIVE},
  {&cg_teamChatsOnly, "cg_teamChatsOnly", "0", CVAR_ARCHIVE},
  {&cg_noVoiceChats, "cg_noVoiceChats", "0", CVAR_ARCHIVE},
  {&cg_noVoiceText, "cg_noVoiceText", "0", CVAR_ARCHIVE},
  // the following variables are created in other parts of the system,
  // but we also reference them here
  {&cg_buildScript, "com_buildScript", "0", 0},	// force loading of all possible data amd error on failures
  {&cg_paused, "cl_paused", "0", CVAR_ROM},
  {&cg_blood, "com_blood", "1", CVAR_ARCHIVE},
  {&cg_synchronousClients, "g_synchronousClients", "0", 0},	// communicated by systeminfo
  {&cg_cameraOrbit, "cg_cameraOrbit", "0", CVAR_CHEAT},
  {&cg_cameraOrbitDelay, "cg_cameraOrbitDelay", "50", CVAR_ARCHIVE},
  {&cg_timescaleFadeEnd, "cg_timescaleFadeEnd", "1", 0},
  {&cg_timescaleFadeSpeed, "cg_timescaleFadeSpeed", "0", 0},
  {&cg_timescale, "timescale", "1", 0},
  {&cg_scorePlum, "cg_scorePlums", "1", CVAR_USERINFO | CVAR_ARCHIVE},

//unlagged - smooth clients #2
//unlagged this is done server-side now
//  {&cg_smoothClients, "cg_smoothClients", "0", CVAR_USERINFO | CVAR_ARCHIVE},
//unlagged - smooth clients #2

  {&cg_cameraMode, "com_cameraMode", "0", CVAR_CHEAT},

  {&pmove_fixed, "pmove_fixed", "0", 0},
  {&pmove_msec, "pmove_msec", "8", 0},
  {&cg_noTaunt, "cg_noTaunt", "0", CVAR_ARCHIVE},
  {&cg_noProjectileTrail, "cg_noProjectileTrail", "0", CVAR_ARCHIVE},
  {&cg_smallFont, "ui_smallFont", "0.25", CVAR_ARCHIVE},
  {&cg_bigFont, "ui_bigFont", "0.4", CVAR_ARCHIVE},
  {&cg_oldRail, "cg_oldRail", "1", CVAR_ARCHIVE},
  {&cg_oldRocket, "cg_oldRocket", "1", CVAR_ARCHIVE},
  {&cg_oldPlasma, "cg_oldPlasma", "1", CVAR_ARCHIVE},
  {&cg_trueLightning, "cg_trueLightning", "0.0", CVAR_ARCHIVE},

//unlagged - client options
  {&cg_delag, "cg_delag", "1", CVAR_ARCHIVE | CVAR_USERINFO},
  {&cg_cmdTimeNudge, "cg_cmdTimeNudge", "0", CVAR_ARCHIVE | CVAR_USERINFO},
  // this will be automagically copied from the server
  {&sv_fps, "sv_fps", "20", 0},
  {&cg_projectileNudge, "cg_projectileNudge", "0", CVAR_ARCHIVE},
  {&cg_optimizePrediction, "cg_optimizePrediction", "1", CVAR_ARCHIVE},
  {&cl_timeNudge, "cl_timeNudge", "0", CVAR_ARCHIVE},
//unlagged - client options

// _ML_ added - new_zoom
  {&cg_newZoom, "cg_newZoom", "0", CVAR_ARCHIVE},
// _ML_ added end

// _ML_ added - crosshair_color
  {&cg_crosshairColor, "cg_crosshairColor", "", CVAR_ARCHIVE},
// _ML_ added end

// _ML_ added - weapon_select
  {&cg_newWeaponSelect, "cg_newWeaponSelect", "0", CVAR_ARCHIVE},
  {&cg_weaponRank, "cg_weaponRank", "", CVAR_ARCHIVE},
  {&cg_weaponGroup1, "cg_weaponGroup1", "", CVAR_ARCHIVE},
  {&cg_weaponGroup2, "cg_weaponGroup2", "", CVAR_ARCHIVE},
  {&cg_weaponGroup3, "cg_weaponGroup3", "", CVAR_ARCHIVE},
  {&cg_weaponGroup4, "cg_weaponGroup4", "", CVAR_ARCHIVE},
// _ML_ added end

// _ML_ added - no_itembob
  {&cg_noBob, "cg_noBob", "0", CVAR_ARCHIVE},
// _ML_ added end

// _ML_ added - no_medals
  {&cg_noMedals, "cg_noMedals", "0", CVAR_ARCHIVE},
// _ML_ added end

// _ML_ added - no_medals
  {&cg_femaleAnnouncer, "cg_femaleAnnouncer", "0", 0},
// _ML_ added end

//mathieu - show_date_time
  {&cg_drawDateTime, "cg_drawDateTime", "0", CVAR_ARCHIVE},
//mathieu

//uZu - frag_message
  {&cg_fragString, "cg_fragString", "You fragged", CVAR_ARCHIVE},
//uZu

//a_clown - speed_meter
  {&cg_drawSpeed, "cg_drawSpeed", "0", CVAR_ARCHIVE},
//a_clown

//hit_sound
  {&cg_hitsound, "cg_hitsound", "1", CVAR_ARCHIVE},
//hit_sound

// _ML_ added - pm_skins
  {&cg_enemyColors, "cg_enemyColors", DEFAULT_ENEMY_COLOR_STRING,
   CVAR_ARCHIVE},
  {&cg_teamColors, "cg_teamColors", DEFAULT_TEAM_COLOR_STRING, CVAR_ARCHIVE},
  {&cg_enemyModel, "cg_enemyModel", DEFAULT_ENEMY_MODEL_STRING, CVAR_ARCHIVE},
  {&cg_teamModel, "cg_teamModel", DEFAULT_TEAM_MODEL_STRING, CVAR_ARCHIVE},
// _ML_ added end

};

static int cvarTableSize = sizeof(cvarTable) / sizeof(cvarTable[0]);

/*
=================
CG_RegisterCvars
=================
*/
void
CG_RegisterCvars(void)
{
  int i;
  cvarTable_t *cv;
  char var[MAX_TOKEN_CHARS];

  for (i = 0, cv = cvarTable; i < cvarTableSize; i++, cv++)
  {
    trap_Cvar_Register(cv->vmCvar, cv->cvarName,
		       cv->defaultString, cv->cvarFlags);
  }

  // see if we are also running the server on this machine
  trap_Cvar_VariableStringBuffer("sv_running", var, sizeof(var));
  cgs.localServer = atoi(var);

  forceModelModificationCount = cg_forceModel.modificationCount;

  trap_Cvar_Register(NULL, "model", DEFAULT_MODEL,
		     CVAR_USERINFO | CVAR_ARCHIVE);
  trap_Cvar_Register(NULL, "headmodel", DEFAULT_MODEL,
		     CVAR_USERINFO | CVAR_ARCHIVE);
  trap_Cvar_Register(NULL, "team_model", DEFAULT_TEAM_MODEL,
		     CVAR_USERINFO | CVAR_ARCHIVE);
  trap_Cvar_Register(NULL, "team_headmodel", DEFAULT_TEAM_HEAD,
		     CVAR_USERINFO | CVAR_ARCHIVE);

// _ML_ added - pm_skins
  Com_LowerCase(cg_enemyModel.string);
  Com_LowerCase(cg_teamModel.string);
// _ML_ added - pm_skins

}

/*											
===================
CG_ForceModelChange
===================
*/
static void
CG_ForceModelChange(void)
{
  int i;

  for (i = 0; i < MAX_CLIENTS; i++)
  {
    const char *clientInfo;

    clientInfo = CG_ConfigString(CS_PLAYERS + i);
    if (!clientInfo[0])
    {
      continue;
    }
    CG_NewClientInfo(i);
  }
}

/*
=================
CG_UpdateCvars
=================
*/
void
CG_UpdateCvars(void)
{
  int i;
  cvarTable_t *cv;

  for (i = 0, cv = cvarTable; i < cvarTableSize; i++, cv++)
  {
//unlagged - client options
    // clamp the value between 0 and 999
    // negative values would suck - people could conceivably shoot other
    // players *long* after they had left the area, on purpose
    if (cv->vmCvar == &cg_cmdTimeNudge)
    {
      CG_Cvar_ClampInt(cv->cvarName, cv->vmCvar, 0, 999);
    }

    // cl_timenudge less than -50 or greater than 50 doesn't actually
    // do anything more than -50 or 50 (actually the numbers are probably
    // closer to -30 and 30, but 50 is nice and round-ish)
    // might as well not feed the myth, eh?
    else if (cv->vmCvar == &cl_timeNudge)
    {
      CG_Cvar_ClampInt(cv->cvarName, cv->vmCvar, -50, 50);
    }
//unlagged - client options

    trap_Cvar_Update(cv->vmCvar);
  }

  // check for modications here

  // If team overlay is on, ask for updates from the server.  If its off,
  // let the server know so we don't receive it
  if (drawTeamOverlayModificationCount !=
      cg_drawTeamOverlay.modificationCount)
  {
    drawTeamOverlayModificationCount = cg_drawTeamOverlay.modificationCount;

    if (cg_drawTeamOverlay.integer > 0)
    {
      trap_Cvar_Set("teamoverlay", "1");
    }
    else
    {
      trap_Cvar_Set("teamoverlay", "0");
    }
  }

  // if force model changed
  if (forceModelModificationCount != cg_forceModel.modificationCount)
  {
    forceModelModificationCount = cg_forceModel.modificationCount;
    CG_ForceModelChange();
  }
}

int
CG_CrosshairPlayer(void)
{
  if (cg.time > (cg.crosshairClientTime + 1000))
  {
    return -1;
  }
  return cg.crosshairClientNum;
}

int
CG_LastAttacker(void)
{
  if (!cg.attackerTime)
  {
    return -1;
  }
  return cg.snap->ps.persistant[PERS_ATTACKER];
}

void QDECL
CG_Printf(const char *msg, ...)
{
  va_list argptr;
  char text[1024];

  va_start(argptr, msg);
  Q_vsnprintf(text, sizeof(text), msg, argptr);
  va_end(argptr);

  trap_Print(text);
}

void QDECL
CG_Error(const char *msg, ...)
{
  va_list argptr;
  char text[1024];

  va_start(argptr, msg);
  Q_vsnprintf(text, sizeof(text), msg, argptr);
  va_end(argptr);

  trap_Error(text);
}

void QDECL
Com_Error(int level, const char *error, ...)
{
  va_list argptr;
  char text[1024];

  va_start(argptr, error);
  Q_vsnprintf(text, sizeof(text), error, argptr);
  va_end(argptr);

  CG_Error("%s", text);
}

void QDECL
Com_Printf(const char *msg, ...)
{
  va_list argptr;
  char text[1024];

  va_start(argptr, msg);
  Q_vsnprintf(text, sizeof(text), msg, argptr);
  va_end(argptr);

  CG_Printf("%s", text);
}

/*
================
CG_Argv
================
*/
const char *
CG_Argv(int arg)
{
  static char buffer[MAX_STRING_CHARS];

  trap_Argv(arg, buffer, sizeof(buffer));

  return buffer;
}


//========================================================================

/*
=================
CG_RegisterItemSounds

The server says this item is used on this level
=================
*/
static void
CG_RegisterItemSounds(int itemNum)
{
  gitem_t *item;
  char data[MAX_QPATH];
  char *s, *start;
  int len;

  item = &bg_itemlist[itemNum];

  if (item->pickup_sound)
  {
    trap_S_RegisterSound(item->pickup_sound, qfalse);
  }

  // parse the space seperated precache string for other media
  s = item->sounds;
  if (!s || !s[0])
    return;

  while (*s)
  {
    start = s;
    while (*s && *s != ' ')
    {
      s++;
    }

    len = s - start;
    if (len >= MAX_QPATH || len < 5)
    {
      CG_Error("PrecacheItem: %s has bad precache string", item->classname);
      return;
    }
    memcpy(data, start, len);
    data[len] = 0;
    if (*s)
    {
      s++;
    }

    if (!strcmp(data + len - 3, "wav"))
    {
      trap_S_RegisterSound(data, qfalse);
    }
  }
}

// _ML_ added - fem_announcer
static sfxHandle_t
reg_feedback_sound(int idx)
{
  char path[MAX_QPATH];

  strcpy(path, voc_feed_dir);
  if (cg_femaleAnnouncer.integer)
    strcat(path, "female/");
  strcat(path, voc_feedback[idx]);

  return trap_S_RegisterSound(path, qtrue);
}

static sfxHandle_t
reg_teamplay_sound(int idx)
{
  char path[MAX_QPATH];

  strcpy(path, voc_team_dir);
  if (cg_femaleAnnouncer.integer)
    strcat(path, "female/");
  strcat(path, voc_teamplay[idx]);

  return trap_S_RegisterSound(path, qtrue);
}

// _ML_ added end

/*
=================
CG_RegisterSounds

called during a precache command
=================
*/

static void
CG_RegisterSounds(void)
{
  int i;
  char items[MAX_ITEMS + 1];
  char name[MAX_QPATH];
  const char *soundName;

  // voice commands

// _ML_ modified - fem_announcer
  cgs.media.oneMinuteSound = reg_feedback_sound(VOC_FEED_1MIN);

  cgs.media.fiveMinuteSound = reg_feedback_sound(VOC_FEED_5MIN);

  cgs.media.suddenDeathSound = reg_feedback_sound(VOC_FEED_SUDDEN);

  cgs.media.oneFragSound = reg_feedback_sound(VOC_FEED_1FRAG);

  cgs.media.twoFragSound = reg_feedback_sound(VOC_FEED_2FRAG);

  cgs.media.threeFragSound = reg_feedback_sound(VOC_FEED_3FRAG);

  cgs.media.count3Sound = reg_feedback_sound(VOC_FEED_THREE);

  cgs.media.count2Sound = reg_feedback_sound(VOC_FEED_TWO);

  cgs.media.count1Sound = reg_feedback_sound(VOC_FEED_ONE);

  cgs.media.countFightSound = reg_feedback_sound(VOC_FEED_FIGHT);

  cgs.media.countPrepareSound = reg_feedback_sound(VOC_FEED_PREPARE);

  if (cgs.gametype >= GT_TEAM || cg_buildScript.integer)
  {
    cgs.media.redLeadsSound = reg_feedback_sound(VOC_FEED_RED_LEAD);

    cgs.media.blueLeadsSound = reg_feedback_sound(VOC_FEED_BLU_LEAD);

    cgs.media.teamsTiedSound = reg_feedback_sound(VOC_FEED_TEAMS_TIED);

    cgs.media.redScoredSound = reg_teamplay_sound(VOC_TEAM_RED_SCORE);

    cgs.media.blueScoredSound = reg_teamplay_sound(VOC_TEAM_BLU_SCORE);
// _ML_ modified end

    cgs.media.captureAwardSound =
      trap_S_RegisterSound("sound/teamplay/flagcapture_yourteam.wav", qtrue);

    cgs.media.hitTeamSound =
      trap_S_RegisterSound("sound/feedback/hit_teammate.wav", qtrue);

    cgs.media.captureYourTeamSound =
      trap_S_RegisterSound("sound/teamplay/flagcapture_yourteam.wav", qtrue);

    cgs.media.captureOpponentSound =
      trap_S_RegisterSound("sound/teamplay/flagcapture_opponent.wav", qtrue);

    cgs.media.returnYourTeamSound =
      trap_S_RegisterSound("sound/teamplay/flagreturn_yourteam.wav", qtrue);

    cgs.media.returnOpponentSound =
      trap_S_RegisterSound("sound/teamplay/flagreturn_opponent.wav", qtrue);

    cgs.media.takenYourTeamSound =
      trap_S_RegisterSound("sound/teamplay/flagtaken_yourteam.wav", qtrue);

    cgs.media.takenOpponentSound =
      trap_S_RegisterSound("sound/teamplay/flagtaken_opponent.wav", qtrue);

    if (cgs.gametype == GT_CTF || cg_buildScript.integer)
    {

// _ML_ modified - fem_announcer
      cgs.media.redFlagReturnedSound = reg_teamplay_sound(VOC_TEAM_RED_RET);

      cgs.media.blueFlagReturnedSound = reg_teamplay_sound(VOC_TEAM_BLU_RET);

      cgs.media.enemyTookYourFlagSound =
	reg_teamplay_sound(VOC_TEAM_ENEMY_FLAG);

      cgs.media.yourTeamTookEnemyFlagSound =
	reg_teamplay_sound(VOC_TEAM_TEAM_FLAG);
    }

    cgs.media.yourTeamTookTheFlagSound =
      reg_teamplay_sound(VOC_TEAM_TEAM_1FLAG);

    cgs.media.enemyTookTheFlagSound =
      reg_teamplay_sound(VOC_TEAM_ENEMY_1FLAG);

    cgs.media.youHaveFlagSound = reg_teamplay_sound(VOC_TEAM_YOU_FLAG);

    cgs.media.holyShitSound = reg_feedback_sound(VOC_FEED_HOLYSHIT);
// _ML_ modified - fem_announcer

    cgs.media.neutralFlagReturnedSound =
      trap_S_RegisterSound("sound/teamplay/flagreturn_opponent.wav", qtrue);
  }

  cgs.media.tracerSound =
    trap_S_RegisterSound("sound/weapons/machinegun/buletby1.wav", qfalse);
  cgs.media.selectSound =
    trap_S_RegisterSound("sound/weapons/change.wav", qfalse);
  cgs.media.wearOffSound =
    trap_S_RegisterSound("sound/items/wearoff.wav", qfalse);
  cgs.media.useNothingSound =
    trap_S_RegisterSound("sound/items/use_nothing.wav", qfalse);
  cgs.media.gibSound =
    trap_S_RegisterSound("sound/player/gibsplt1.wav", qfalse);
  cgs.media.gibBounce1Sound =
    trap_S_RegisterSound("sound/player/gibimp1.wav", qfalse);
  cgs.media.gibBounce2Sound =
    trap_S_RegisterSound("sound/player/gibimp2.wav", qfalse);
  cgs.media.gibBounce3Sound =
    trap_S_RegisterSound("sound/player/gibimp3.wav", qfalse);
  cgs.media.teleInSound =
    trap_S_RegisterSound("sound/world/telein.wav", qfalse);
  cgs.media.teleOutSound =
    trap_S_RegisterSound("sound/world/teleout.wav", qfalse);
  cgs.media.respawnSound =
    trap_S_RegisterSound("sound/items/respawn1.wav", qfalse);
  cgs.media.noAmmoSound =
    trap_S_RegisterSound("sound/weapons/noammo.wav", qfalse);
  cgs.media.talkSound = trap_S_RegisterSound("sound/player/talk.wav", qfalse);
  cgs.media.landSound =
    trap_S_RegisterSound("sound/player/land1.wav", qfalse);

  cgs.media.hitSound = trap_S_RegisterSound("sound/feedback/hit.wav", qfalse);

//hit_sound
  cgs.media.hitSoundHigh =
    trap_S_RegisterSound("sound/feedback/hit_high.wav", qfalse);
  cgs.media.hitSoundLow =
    trap_S_RegisterSound("sound/feedback/hit_low.wav", qfalse);
//hit_sound

// _ML_ modified - fem_announcer
  cgs.media.impressiveSound = reg_feedback_sound(VOC_FEED_IMPRESSIVE);

  cgs.media.excellentSound = reg_feedback_sound(VOC_FEED_EXCELLENT);

  cgs.media.deniedSound = reg_feedback_sound(VOC_FEED_DENIED);

  cgs.media.humiliationSound = reg_feedback_sound(VOC_FEED_HUMILIATION);

  cgs.media.assistSound = reg_feedback_sound(VOC_FEED_ASSIST);

  cgs.media.defendSound = reg_feedback_sound(VOC_FEED_DEFENSE);

  cgs.media.takenLeadSound = reg_feedback_sound(VOC_FEED_TAKE_LEAD);

  cgs.media.tiedLeadSound = reg_feedback_sound(VOC_FEED_TIED);

  cgs.media.lostLeadSound = reg_feedback_sound(VOC_FEED_LOST_LEAD);
// _ML_ modified end

  cgs.media.watrInSound =
    trap_S_RegisterSound("sound/player/watr_in.wav", qfalse);
  cgs.media.watrOutSound =
    trap_S_RegisterSound("sound/player/watr_out.wav", qfalse);
  cgs.media.watrUnSound =
    trap_S_RegisterSound("sound/player/watr_un.wav", qfalse);

  cgs.media.jumpPadSound =
    trap_S_RegisterSound("sound/world/jumppad.wav", qfalse);

  for (i = 0; i < 4; i++)
  {
    Com_sprintf(name, sizeof(name), "sound/player/footsteps/step%i.wav",
		i + 1);
    cgs.media.footsteps[FOOTSTEP_NORMAL][i] =
      trap_S_RegisterSound(name, qfalse);

    Com_sprintf(name, sizeof(name), "sound/player/footsteps/boot%i.wav",
		i + 1);
    cgs.media.footsteps[FOOTSTEP_BOOT][i] =
      trap_S_RegisterSound(name, qfalse);

    Com_sprintf(name, sizeof(name), "sound/player/footsteps/flesh%i.wav",
		i + 1);
    cgs.media.footsteps[FOOTSTEP_FLESH][i] =
      trap_S_RegisterSound(name, qfalse);

    Com_sprintf(name, sizeof(name), "sound/player/footsteps/mech%i.wav",
		i + 1);
    cgs.media.footsteps[FOOTSTEP_MECH][i] =
      trap_S_RegisterSound(name, qfalse);

    Com_sprintf(name, sizeof(name), "sound/player/footsteps/energy%i.wav",
		i + 1);
    cgs.media.footsteps[FOOTSTEP_ENERGY][i] =
      trap_S_RegisterSound(name, qfalse);

    Com_sprintf(name, sizeof(name), "sound/player/footsteps/splash%i.wav",
		i + 1);
    cgs.media.footsteps[FOOTSTEP_SPLASH][i] =
      trap_S_RegisterSound(name, qfalse);

    Com_sprintf(name, sizeof(name), "sound/player/footsteps/clank%i.wav",
		i + 1);
    cgs.media.footsteps[FOOTSTEP_METAL][i] =
      trap_S_RegisterSound(name, qfalse);
  }

  // only register the items that the server says we need
  Q_strncpyz(items, CG_ConfigString(CS_ITEMS), sizeof(items));

  for (i = 1; i < bg_numItems; i++)
    CG_RegisterItemSounds(i);

  for (i = 1; i < MAX_SOUNDS; i++)
  {
    soundName = CG_ConfigString(CS_SOUNDS + i);
    if (!soundName[0])
    {
      break;
    }
    if (soundName[0] == '*')
    {
      continue;			// custom sound
    }
    cgs.gameSounds[i] = trap_S_RegisterSound(soundName, qfalse);
  }

  // FIXME: only needed with item
  cgs.media.flightSound =
    trap_S_RegisterSound("sound/items/flight.wav", qfalse);
  cgs.media.medkitSound =
    trap_S_RegisterSound("sound/items/use_medkit.wav", qfalse);
  cgs.media.quadSound =
    trap_S_RegisterSound("sound/items/damage3.wav", qfalse);
  cgs.media.sfx_ric1 =
    trap_S_RegisterSound("sound/weapons/machinegun/ric1.wav", qfalse);
  cgs.media.sfx_ric2 =
    trap_S_RegisterSound("sound/weapons/machinegun/ric2.wav", qfalse);
  cgs.media.sfx_ric3 =
    trap_S_RegisterSound("sound/weapons/machinegun/ric3.wav", qfalse);
  cgs.media.sfx_railg =
    trap_S_RegisterSound("sound/weapons/railgun/railgf1a.wav", qfalse);
  cgs.media.sfx_rockexp =
    trap_S_RegisterSound("sound/weapons/rocket/rocklx1a.wav", qfalse);
  cgs.media.sfx_plasmaexp =
    trap_S_RegisterSound("sound/weapons/plasma/plasmx1a.wav", qfalse);
  cgs.media.regenSound =
    trap_S_RegisterSound("sound/items/regen.wav", qfalse);
  cgs.media.protectSound =
    trap_S_RegisterSound("sound/items/protect3.wav", qfalse);
  cgs.media.n_healthSound =
    trap_S_RegisterSound("sound/items/n_health.wav", qfalse);
  cgs.media.hgrenb1aSound =
    trap_S_RegisterSound("sound/weapons/grenade/hgrenb1a.wav", qfalse);
  cgs.media.hgrenb2aSound =
    trap_S_RegisterSound("sound/weapons/grenade/hgrenb2a.wav", qfalse);
}


//===================================================================================


/*
=================
CG_RegisterGraphics

This function may execute for a couple of minutes with a slow disk.
=================
*/
static void
CG_RegisterGraphics(void)
{
  int i;
  char items[MAX_ITEMS + 1];
  static char *sb_nums[11] = {
    "gfx/2d/numbers/zero_32b",
    "gfx/2d/numbers/one_32b",
    "gfx/2d/numbers/two_32b",
    "gfx/2d/numbers/three_32b",
    "gfx/2d/numbers/four_32b",
    "gfx/2d/numbers/five_32b",
    "gfx/2d/numbers/six_32b",
    "gfx/2d/numbers/seven_32b",
    "gfx/2d/numbers/eight_32b",
    "gfx/2d/numbers/nine_32b",
    "gfx/2d/numbers/minus_32b",
  };

  // clear any references to old media
  memset(&cg.refdef, 0, sizeof(cg.refdef));
  trap_R_ClearScene();

  CG_LoadingString(cgs.mapname);

  trap_R_LoadWorldMap(cgs.mapname);

  // precache status bar pics
  CG_LoadingString("game media");

  for (i = 0; i < 11; i++)
  {
    cgs.media.numberShaders[i] = trap_R_RegisterShader(sb_nums[i]);
  }

  cgs.media.botSkillShaders[0] = trap_R_RegisterShader("menu/art/skill1.tga");
  cgs.media.botSkillShaders[1] = trap_R_RegisterShader("menu/art/skill2.tga");
  cgs.media.botSkillShaders[2] = trap_R_RegisterShader("menu/art/skill3.tga");
  cgs.media.botSkillShaders[3] = trap_R_RegisterShader("menu/art/skill4.tga");
  cgs.media.botSkillShaders[4] = trap_R_RegisterShader("menu/art/skill5.tga");

  cgs.media.viewBloodShader = trap_R_RegisterShader("viewBloodBlend");

  cgs.media.deferShader = trap_R_RegisterShaderNoMip("gfx/2d/defer.tga");

  cgs.media.scoreboardName = trap_R_RegisterShaderNoMip("menu/tab/name.tga");
  cgs.media.scoreboardPing = trap_R_RegisterShaderNoMip("menu/tab/ping.tga");
  cgs.media.scoreboardScore =
    trap_R_RegisterShaderNoMip("menu/tab/score.tga");
  cgs.media.scoreboardTime = trap_R_RegisterShaderNoMip("menu/tab/time.tga");

  cgs.media.smokePuffShader = trap_R_RegisterShader("smokePuff");
  cgs.media.smokePuffRageProShader =
    trap_R_RegisterShader("smokePuffRagePro");
  cgs.media.shotgunSmokePuffShader =
    trap_R_RegisterShader("shotgunSmokePuff");
  cgs.media.plasmaBallShader = trap_R_RegisterShader("sprites/plasma1");
  cgs.media.bloodTrailShader = trap_R_RegisterShader("bloodTrail");
  cgs.media.lagometerShader = trap_R_RegisterShader("lagometer");
  cgs.media.connectionShader = trap_R_RegisterShader("disconnected");

  cgs.media.waterBubbleShader = trap_R_RegisterShader("waterBubble");

  cgs.media.tracerShader = trap_R_RegisterShader("gfx/misc/tracer");
  cgs.media.selectShader = trap_R_RegisterShader("gfx/2d/select");

// _ML_ modified - crosshair_fix
  for (i = 0; i < NUM_CROSSHAIRS; i++)
  {
    cgs.media.crosshairShader[i] =
      trap_R_RegisterShader(va("gfx/2d/crosshair%s", crosshair_num[i]));
  }
// _ML_ modified end

// _ML_ added - new_zoom
  cgs.media.zoom_crossShader = trap_R_RegisterShader("gfx/2d/zoomcross");
// _ML_ added end

// _ML_ added - spawn_protect
  cgs.media.protCrossShader = trap_R_RegisterShader("gfx/2d/prot_cross");
  cgs.media.protIconShader = trap_R_RegisterShaderNoMip("prot_icon");
// _ML_ added end

  cgs.media.backTileShader = trap_R_RegisterShader("gfx/2d/backtile");
  cgs.media.noammoShader = trap_R_RegisterShader("icons/noammo");

  // powerup shaders
  cgs.media.quadShader = trap_R_RegisterShader("powerups/quad");
  cgs.media.quadWeaponShader = trap_R_RegisterShader("powerups/quadWeapon");
  cgs.media.battleSuitShader = trap_R_RegisterShader("powerups/battleSuit");
  cgs.media.battleWeaponShader =
    trap_R_RegisterShader("powerups/battleWeapon");
  cgs.media.invisShader = trap_R_RegisterShader("powerups/invisibility");
  cgs.media.regenShader = trap_R_RegisterShader("powerups/regen");
  cgs.media.hastePuffShader = trap_R_RegisterShader("hasteSmokePuff");

  if (cgs.gametype == GT_CTF || cg_buildScript.integer)
  {
    cgs.media.redCubeModel =
      trap_R_RegisterModel("models/powerups/orb/r_orb.md3");
    cgs.media.blueCubeModel =
      trap_R_RegisterModel("models/powerups/orb/b_orb.md3");
    cgs.media.redCubeIcon = trap_R_RegisterShader("icons/skull_red");
    cgs.media.blueCubeIcon = trap_R_RegisterShader("icons/skull_blue");
  }

  if (cgs.gametype == GT_CTF || cg_buildScript.integer)
  {
    cgs.media.redFlagModel = trap_R_RegisterModel("models/flags/r_flag.md3");
    cgs.media.blueFlagModel = trap_R_RegisterModel("models/flags/b_flag.md3");
    cgs.media.redFlagShader[0] =
      trap_R_RegisterShaderNoMip("icons/iconf_red1");
    cgs.media.redFlagShader[1] =
      trap_R_RegisterShaderNoMip("icons/iconf_red2");
    cgs.media.redFlagShader[2] =
      trap_R_RegisterShaderNoMip("icons/iconf_red3");
    cgs.media.blueFlagShader[0] =
      trap_R_RegisterShaderNoMip("icons/iconf_blu1");
    cgs.media.blueFlagShader[1] =
      trap_R_RegisterShaderNoMip("icons/iconf_blu2");
    cgs.media.blueFlagShader[2] =
      trap_R_RegisterShaderNoMip("icons/iconf_blu3");
  }

  if (cgs.gametype >= GT_TEAM || cg_buildScript.integer)
  {
    cgs.media.friendShader = trap_R_RegisterShader("sprites/foe");
    cgs.media.redQuadShader = trap_R_RegisterShader("powerups/blueflag");
    cgs.media.teamStatusBar = trap_R_RegisterShader("gfx/2d/colorbar.tga");
  }

  cgs.media.armorModel =
    trap_R_RegisterModel("models/powerups/armor/armor_yel.md3");
  cgs.media.armorIcon = trap_R_RegisterShaderNoMip("icons/iconr_yellow");

  cgs.media.machinegunBrassModel =
    trap_R_RegisterModel("models/weapons2/shells/m_shell.md3");
  cgs.media.shotgunBrassModel =
    trap_R_RegisterModel("models/weapons2/shells/s_shell.md3");

  cgs.media.gibAbdomen = trap_R_RegisterModel("models/gibs/abdomen.md3");
  cgs.media.gibArm = trap_R_RegisterModel("models/gibs/arm.md3");
  cgs.media.gibChest = trap_R_RegisterModel("models/gibs/chest.md3");
  cgs.media.gibFist = trap_R_RegisterModel("models/gibs/fist.md3");
  cgs.media.gibFoot = trap_R_RegisterModel("models/gibs/foot.md3");
  cgs.media.gibForearm = trap_R_RegisterModel("models/gibs/forearm.md3");
  cgs.media.gibIntestine = trap_R_RegisterModel("models/gibs/intestine.md3");
  cgs.media.gibLeg = trap_R_RegisterModel("models/gibs/leg.md3");
  cgs.media.gibSkull = trap_R_RegisterModel("models/gibs/skull.md3");
  cgs.media.gibBrain = trap_R_RegisterModel("models/gibs/brain.md3");

  cgs.media.smoke2 =
    trap_R_RegisterModel("models/weapons2/shells/s_shell.md3");

  cgs.media.balloonShader = trap_R_RegisterShader("sprites/balloon3");

  cgs.media.bloodExplosionShader = trap_R_RegisterShader("bloodExplosion");

  cgs.media.bulletFlashModel =
    trap_R_RegisterModel("models/weaphits/bullet.md3");
  cgs.media.ringFlashModel =
    trap_R_RegisterModel("models/weaphits/ring02.md3");
  cgs.media.dishFlashModel =
    trap_R_RegisterModel("models/weaphits/boom01.md3");
  cgs.media.teleportEffectModel =
    trap_R_RegisterModel("models/misc/telep.md3");
  cgs.media.teleportEffectShader = trap_R_RegisterShader("teleportEffect");

  cgs.media.invulnerabilityPowerupModel =
    trap_R_RegisterModel("models/powerups/shield/shield.md3");
  cgs.media.medalImpressive = trap_R_RegisterShaderNoMip("medal_impressive");
  cgs.media.medalExcellent = trap_R_RegisterShaderNoMip("medal_excellent");
  cgs.media.medalGauntlet = trap_R_RegisterShaderNoMip("medal_gauntlet");
  cgs.media.medalDefend = trap_R_RegisterShaderNoMip("medal_defend");
  cgs.media.medalAssist = trap_R_RegisterShaderNoMip("medal_assist");
  cgs.media.medalCapture = trap_R_RegisterShaderNoMip("medal_capture");


  memset(cg_items, 0, sizeof(cg_items));
  memset(cg_weapons, 0, sizeof(cg_weapons));

  // only register the items that the server says we need
  Q_strncpyz(items, CG_ConfigString(CS_ITEMS), sizeof(items));

  for (i = 1; i < bg_numItems; i++)
  {
    if (items[i] == '1' || cg_buildScript.integer)
    {
      CG_LoadingItem(i);
      CG_RegisterItemVisuals(i);
    }
  }

  // wall marks
  cgs.media.bulletMarkShader = trap_R_RegisterShader("gfx/damage/bullet_mrk");
  cgs.media.burnMarkShader = trap_R_RegisterShader("gfx/damage/burn_med_mrk");
  cgs.media.holeMarkShader = trap_R_RegisterShader("gfx/damage/hole_lg_mrk");
  cgs.media.energyMarkShader = trap_R_RegisterShader("gfx/damage/plasma_mrk");
  cgs.media.shadowMarkShader = trap_R_RegisterShader("markShadow");
  cgs.media.wakeMarkShader = trap_R_RegisterShader("wake");
  cgs.media.bloodMarkShader = trap_R_RegisterShader("bloodMark");

  // register the inline models
  cgs.numInlineModels = trap_CM_NumInlineModels();
  for (i = 1; i < cgs.numInlineModels; i++)
  {
    char name[10];
    vec3_t mins, maxs;
    int j;

    Com_sprintf(name, sizeof(name), "*%i", i);
    cgs.inlineDrawModel[i] = trap_R_RegisterModel(name);
    trap_R_ModelBounds(cgs.inlineDrawModel[i], mins, maxs);
    for (j = 0; j < 3; j++)
    {
      cgs.inlineModelMidpoints[i][j] = mins[j] + 0.5 * (maxs[j] - mins[j]);
    }
  }

  // register all the server specified models
  for (i = 1; i < MAX_MODELS; i++)
  {
    const char *modelName;

    modelName = CG_ConfigString(CS_MODELS + i);
    if (!modelName[0])
    {
      break;
    }
    cgs.gameModels[i] = trap_R_RegisterModel(modelName);
  }

  CG_ClearParticles();
}



/*																																			
=======================
CG_BuildSpectatorString

=======================
*/
void
CG_BuildSpectatorString(void)
{
  int i;
  cg.spectatorList[0] = 0;
  for (i = 0; i < MAX_CLIENTS; i++)
  {
    if (cgs.clientinfo[i].infoValid
	&& cgs.clientinfo[i].team == TEAM_SPECTATOR)
    {
      Q_strcat(cg.spectatorList, sizeof(cg.spectatorList),
	       va("%s     ", cgs.clientinfo[i].name));
    }
  }
  i = strlen(cg.spectatorList);
  if (i != cg.spectatorLen)
  {
    cg.spectatorLen = i;
    cg.spectatorWidth = -1;
  }
}


/*																																			
===================
CG_RegisterClients
===================
*/
static void
CG_RegisterClients(void)
{
  int i;

  CG_LoadingClient(cg.clientNum);
  CG_NewClientInfo(cg.clientNum);

  for (i = 0; i < MAX_CLIENTS; i++)
  {
    const char *clientInfo;

    if (cg.clientNum == i)
    {
      continue;
    }

    clientInfo = CG_ConfigString(CS_PLAYERS + i);
    if (!clientInfo[0])
    {
      continue;
    }
    CG_LoadingClient(i);
    CG_NewClientInfo(i);
  }
  CG_BuildSpectatorString();
}

//===========================================================================

/*
=================
CG_ConfigString
=================
*/
const char *
CG_ConfigString(int index)
{
  if (index < 0 || index >= MAX_CONFIGSTRINGS)
  {
    CG_Error("CG_ConfigString: bad index: %i", index);
  }
  return cgs.gameState.stringData + cgs.gameState.stringOffsets[index];
}

//==================================================================

/*
======================
CG_StartMusic

======================
*/
void
CG_StartMusic(void)
{
  char *s;
  char parm1[MAX_QPATH], parm2[MAX_QPATH];

  // start the background music
  s = (char *) CG_ConfigString(CS_MUSIC);
  Q_strncpyz(parm1, COM_Parse(&s), sizeof(parm1));
  Q_strncpyz(parm2, COM_Parse(&s), sizeof(parm2));

  trap_S_StartBackgroundTrack(parm1, parm2);
}

// _ML_ added - crosshair_color
static int
parse_crosshair_color(char *rgb)
{
  double val;
  char tok[8], *cp;

  // red component
  cp = Com_GetToken(rgb, tok, 7);
  if (cp == NULL)
    return 0;

  if (!Com_Atof(tok, &val))
    return 0;

  if (val < 0.0 || val > 1.0)
    return 0;

  cross_color[0] = val;

  // green component
  cp = Com_GetToken(cp, tok, 7);
  if (cp == NULL)
    return 0;

  if (!Com_Atof(tok, &val))
    return 0;

  if (val < 0.0 || val > 1.0)
    return 0;

  cross_color[1] = val;

  // blue component
  cp = Com_GetToken(cp, tok, 7);
  if (cp == NULL)
    return 0;

  if (!Com_Atof(tok, &val))
    return 0;

  if (val < 0.0 || val > 1.0)
    return 0;

  cross_color[2] = val;
  cross_color[3] = 1;
  return 1;
}

// _ML_ added end

/*
=================
CG_Init

Called after every level change or subsystem restart
Will perform callbacks to make the loading info screen update.
=================
*/
void
CG_Init(int serverMessageNum, int serverCommandSequence, int clientNum)
{
  const char *s;

// _ML_ added - exe_checking
  if (trap_Get_EXE_ID() != EXE_ID)
    CG_Error(EXE_ERR_MSG);
// _ML_ added end

  // clear everything
  memset(&cgs, 0, sizeof(cgs));
  memset(&cg, 0, sizeof(cg));
  memset(cg_entities, 0, sizeof(cg_entities));
  memset(cg_weapons, 0, sizeof(cg_weapons));
  memset(cg_items, 0, sizeof(cg_items));

  cg.clientNum = clientNum;

  cgs.processedSnapshotNum = serverMessageNum;
  cgs.serverCommandSequence = serverCommandSequence;

  // load a few needed things before we do any screen updates
  cgs.media.charsetShader = trap_R_RegisterShader("gfx/2d/bigchars");
  cgs.media.whiteShader = trap_R_RegisterShader("white");
  cgs.media.charsetProp =
    trap_R_RegisterShaderNoMip("menu/art/font1_prop.tga");
  cgs.media.charsetPropGlow =
    trap_R_RegisterShaderNoMip("menu/art/font1_prop_glo.tga");
  cgs.media.charsetPropB =
    trap_R_RegisterShaderNoMip("menu/art/font2_prop.tga");

  CG_RegisterCvars();

// _ML_ added - weapon_select
  if (cg_weaponRank.string[0] != 0)
    CG_ConfigWeapons();
// _ML_ added end

// _ML_ added - crosshair_color
  colored_crosshair = parse_crosshair_color(cg_crosshairColor.string);
// _ML_ added end

  CG_InitConsoleCommands();

  cg.weaponSelect = WP_MACHINEGUN;

  cgs.redflag = cgs.blueflag = -1;	// For compatibily, default to unset for
  cgs.flagStatus = -1;
  // old servers

  // get the rendering configuration from the client system
  trap_GetGlconfig(&cgs.glconfig);
  cgs.screenXScale = cgs.glconfig.vidWidth / 640.0;
  cgs.screenYScale = cgs.glconfig.vidHeight / 480.0;

  // get the gamestate from the client system
  trap_GetGameState(&cgs.gameState);

  // check version
  s = CG_ConfigString(CS_GAME_VERSION);
  if (strcmp(s, GAME_VERSION))
  {
    CG_Error("Client/Server game mismatch: %s/%s", GAME_VERSION, s);
  }

  s = CG_ConfigString(CS_LEVEL_START_TIME);
  cgs.levelStartTime = atoi(s);

  CG_ParseServerinfo();

  // load the new map
  CG_LoadingString("collision map");

  trap_CM_LoadMap(cgs.mapname);

  cg.loading = qtrue;		// force players to load instead of defer

  CG_LoadingString("sounds");

  CG_RegisterSounds();

  CG_LoadingString("graphics");

  CG_RegisterGraphics();

  CG_LoadingString("clients");

  CG_RegisterClients();		// if low on memory, some clients will be deferred

  cg.loading = qfalse;		// future players will be deferred

  CG_InitLocalEntities();

  CG_InitMarkPolys();

  // remove the last loading update
  cg.infoScreenText[0] = 0;

  // Make sure we have update values (scores)
  CG_SetConfigValues();

  CG_StartMusic();

  CG_LoadingString("");

  CG_ShaderStateChanged();

  trap_S_ClearLoopingSounds(qtrue);
}

/*
=================
CG_Shutdown

Called before every level change or subsystem restart
=================
*/
void
CG_Shutdown(void)
{
  // some mods may need to do cleanup work here,
  // like closing files or archiving session data
}


/*
==================
CG_EventHandling
==================
 type 0 - no event handling
      1 - team menu
      2 - hud editor

*/
void
CG_EventHandling(int type)
{
}

void
CG_KeyEvent(int key, qboolean down)
{
}

void
CG_MouseEvent(int x, int y)
{
}

//unlagged - attack prediction #3
// from g_weapon.c

/*
======================
SnapVectorTowards

Round a vector to integers for more efficient network
transmission, but make sure that it rounds towards a given point
rather than blindly truncating.  This prevents it from truncating 
into a wall.
======================
*/
void
SnapVectorTowards(vec3_t v, vec3_t to)
{
  int i;

  for (i = 0; i < 3; i++)
  {
    if (to[i] <= v[i])
    {
      v[i] = (int) v[i];
    }
    else
    {
      v[i] = (int) v[i] + 1;
    }
  }
}

//unlagged - attack prediction #3
