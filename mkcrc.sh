#!/bin/sh
# Copies the CRC32 checksum to the 'pakcrc.h' file.
# Argument: build directory path (relative to current)
# Note: This is only for use as a command inside the Makefile.

PAK_CRC=`./misc/crcgen.exe $1/pak1.pk3`
echo "#define PAK1_CRC 0x"$PAK_CRC > pakcrc.h
