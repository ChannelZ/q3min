#!/bin/sh
# Creates a new 'version.h' file with a unique 8-digit ID for the EXE modules
# Note: This is only for use as a command inside the Makefile.

VERSION_NUM=`cat VERSION`
EXE_ID=`./misc/mkrand.exe 8`

echo "Creating 'version.h'..."
echo "#define EXE_ERR_MSG \"EXE check error: version mismatch\"" > version.h 
echo "#define VERSION_STRING " $VERSION_NUM >> version.h
echo "#define EXE_ID " $EXE_ID >> version.h
