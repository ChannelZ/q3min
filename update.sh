#!/bin/sh
# Copies Q3MIN modules (EXE, VM and PAK) to the install folder.
# The path to the install folder is taken from 'config.local'.
# If it is not defined there, a default is used.
# Note: intended for use as a command in the makefile
# Arguments:
#  $1 = 'release' or 'debug'
#  $2 = 'mingw32'
#  $3 = 'x86'

function error_exit
{
  echo "Q3MIN uopdate script"
  echo $1
  echo "Usage: update MODE PLAT ARCH";
  echo "  MODE = build mode ('release' or 'debug')";
  echo "  PLAT = platform ('mingw32' or 'linux')";
  echo "  ARCH = architecture ('x86', 'i386', 'x86_64' or 'ppc')";
  exit 1;
}

cwd=`pwd`

msysdef=`grep -h MSYS_HOME config.local`
msys=${msysdef#*=*}

if [ -z $msys ]; then
  error_exit "The home path of 'Msys' is not specified"
fi

instdef=`grep -h WIN32_INSTALL_DIR config.local`
inst=${instdef#*=*}

if [ -z $inst ]; then
  inst="c:\Program Files\q3min"
fi

if [ -z $1 ]; then
  error_exit "missing build mode string"
fi

if [ $1 != "release" -a $1 != "debug" ]; then
  error_exit "unknown build mode string"
fi

if [ -z $2 ]; then
  error_exit "missing platform string"
fi

if [ $2 != "mingw32" ]; then
  error_exit "unknown platform string"
fi

if [ -z $3 ]; then
  error_exit "missing architecture string"
fi

if [ $3 != "x86" ]; then
  error_exit "unknown architecture string"
fi

bld="$msys/$cwd/build/$1-$2-$3"

if [ ! -d $src ]; then
  echo "Build directory does not exist";
  exit 1;
fi

if [ ! -d $inst ]; then
  echo "Install directory does not exist";
  exit 1;
fi

client="$bld/q3m.exe"
server="$bld/q3mded.exe"
pak1="$bld/pak1.pk3"

if [ ! -f $client ]; then
  error_exit "missing client module"
fi

if [ ! -f $server ]; then
  error_exit "missing server module"
fi

if [ ! -f $pak1 ]; then
  error_exit "missing pak1.pk3 module"
fi

echo "Updating Q3MIN modules ($1 $2 $3)..."
cp -f $client $inst
cp -f $server $inst
cp -f $pak1 $inst/baseqm
echo "...done"

################
cp -f $client e:/q3min-server
cp -f $server e:/q3min-server
cp -f $pak1 e:/q3min-server/baseqm
################

exit 0
